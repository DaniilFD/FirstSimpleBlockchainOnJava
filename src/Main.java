import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.TimeZone;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        TimeZone tz = TimeZone.getTimeZone("Europe/Moscow");
        Calendar time= Calendar.getInstance(tz);
        time.setLenient(false);
        time.set(2017, Calendar.FEBRUARY, 15, 11, 30, 33);
        Blockchain myFirstCoin = new Blockchain();
        myFirstCoin.addBlock(new Block(1,time,"Second",new byte[0]));
        myFirstCoin.addBlock(new Block(2,time,"Third",new byte[0]));
        myFirstCoin.addBlock(new Block(3,time,"Fourth",new byte[0]));
        System.out.println(myFirstCoin.toString());
    }
}
