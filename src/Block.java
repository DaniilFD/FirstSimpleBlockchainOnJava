import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Calendar;

public class Block {
    private int index;
    private Calendar timestamp; //сделать Calendar
    private String data;

    private byte[] previousHash;
    private byte[] hash;
    Block(int index, Calendar timestamp, String data, byte[] previousHash) throws NoSuchAlgorithmException {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = timestamp;
        this.data = data;
        this.hash = this.calculateHash();
    }
    public byte[] calculateHash() throws NoSuchAlgorithmException {
        String conc = this.getIndex() + this.getTimestamp().toString() + this.getData();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(conc.getBytes());
        return hash;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public byte[] getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(byte[] previousHash) {
        this.previousHash = previousHash;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Block{" + "index=" + index + ", data='" + data + '\'' + ", previousHash='" + previousHash + '\'' + ", hash='" + hash + '\'' + '}' +"\n";
    }
}
