import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Blockchain {
    private List<Block> chain;
    Blockchain() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        this.createGenesisBlock();
    }
    private void createGenesisBlock() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        chain = new ArrayList<Block>();
        TimeZone tz = TimeZone.getTimeZone("Europe/Moscow");
        chain.add(new Block(0, Calendar.getInstance(tz), "Genesis", new byte[0]));
    }
    public Block getLatestBlock() {
        return chain.get(chain.size()-1);
    }

    public void addBlock(Block newBlock) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        newBlock.setPreviousHash(this.getLatestBlock().getHash());
        newBlock.setHash(newBlock.calculateHash());
        chain.add(newBlock);
    }
/*
    public boolean isChainValid() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Iterator<Block> it = this.chain.iterator();
        boolean check=true;
        for (int i=1;i<chain.size();++i){
            Block previousBlock = this.chain.get(i-1);
            Block currentBlock = this.chain.get(i);
            if (currentBlock.calculateHash() != currentBlock.getHash()
                    || previousBlock.getHash() != currentBlock.getPreviousHash()) {
                System.out.println("oops");
                check=false;
            } else {
                check=true;
            }
        }
        return check;
    }*/

    @Override
    public String toString() {
        return "Blockchain: \n" + chain;
    }
}
